[//]: # (# Flutter Pokedex)

[//]: # ()
[//]: # ([![Awesome Flutter]&#40;https://img.shields.io/badge/Awesome-Flutter-blue.svg&#41;]&#40;https://github.com/Solido/awesome-flutter&#41;)

[//]: # ([![Codemagic build status]&#40;https://api.codemagic.io/apps/5d3727997dee22001bb7681c/5d3727997dee22001bb7681b/status_badge.svg&#41;]&#40;https://codemagic.io/apps/5d3727997dee22001bb7681c/5d3727997dee22001bb7681b/latest_build&#41;)

[//]: # ([![License]&#40;https://img.shields.io/badge/License-Apache%202.0-red.svg&#41;]&#40;LICENSE&#41;)

[//]: # ([![License]&#40;https://img.shields.io/badge/License-MIT-red.svg&#41;]&#40;LICENSE&#41;)

[//]: # ()
[//]: # (Pokedex app built with Flutter)

[//]: # ()
[//]: # (## App preview)

[//]: # ()
[//]: # (![Home]&#40;screenshots/home.png "Home"&#41;)

[//]: # (![News]&#40;screenshots/home-news.png "News"&#41;)

[//]: # (![Pokedex]&#40;screenshots/pokedex.png "Pokedex"&#41;)

[//]: # (![Pokedex FAB]&#40;screenshots/pokedex-fab.png "Pokedex FAB"&#41;)

[//]: # (![Pokedex Generation]&#40;screenshots/pokedex-fab-generation.png "Pokedex Generation"&#41;)

[//]: # (![Pokemon Info - About]&#40;screenshots/pokemon-info-about.png "Pokemon Info - About"&#41;)

[//]: # (![Pokemon Info - Base Stats]&#40;screenshots/pokemon-info-base-stats.png "Pokemon Info - Base Stats"&#41;)

[//]: # (![Pokemon Info - Evolution]&#40;screenshots/pokemon-info-evolution.png "Pokemon Info - Evolution"&#41;)

[//]: # (![Pokemon Info - Base Stats &#40;Expanded&#41;]&#40;screenshots/pokemon-info-expanded.png "Pokemon Info - Base Stats &#40;Expanded&#41;"&#41;)

[//]: # ()
[//]: # (## Video demo)

[//]: # ([![Demo]&#40;screenshots/thumbnail.png&#41;]&#40;https://www.youtube.com/watch?v=qKrFWerjoV8&#41;)

[//]: # ()
[//]: # (## Installation)

[//]: # ()
[//]: # (- Add [Flutter]&#40;https://flutter.dev/docs/get-started/install&#41; to your machine)

[//]: # ()
[//]: # (- Open this project folder with Terminal/CMD and run `flutter packages get`)

[//]: # ()
[//]: # (- Run `flutter run` to build and run the debug app on your emulator/phone)

[//]: # ()
[//]: # (## Todos)

[//]: # ()
[//]: # (- [x] Home)

[//]: # (- [x] Home - Apply Sliver effect in home screen)

[//]: # (- [x] Pokedex)

[//]: # (- [x] Pokedex - FAB)

[//]: # (- [x] Pokedex - Add FAB animation)

[//]: # (- [ ] Pokedex - Add grid loading animation)

[//]: # (- [x] Pokedex - Add more Pokemons by [balvinderz]&#40;https://github.com/balvinderz&#41;)

[//]: # (- [x] Pokedex - Add load more)

[//]: # (- [x] Pokemon Info)

[//]: # (- [x] Pokemon Info - About)

[//]: # (- [x] Pokemon Info - Base Stats)

[//]: # (- [x] Pokemon Info - Evolution)

[//]: # (- [ ] Pokemon Info - Moves &#40;no design&#41;)

[//]: # (- [x] Pokemon Info - Make tab area expandable)

[//]: # (- [x] Pokemon Info - Add animations)

[//]: # (- [x] Pokemon Info - Add more Pokemons by [balvinderz]&#40;https://github.com/balvinderz&#41;)

[//]: # (- [ ] Pokemon Info - Add missing data in About tab)

[//]: # (- [x] Pokemon Info - Add Base Stats data by [balvinderz]&#40;https://github.com/balvinderz&#41;)

[//]: # (- [x] Pokemon Info - Add Evolution data by [balvinderz]&#40;https://github.com/balvinderz&#41;)

[//]: # ()
[//]: # (## Thanks)

[//]: # ()
[//]: # (- [Saepul Nahwan]&#40;https://dribbble.com/saepulnahwan23&#41; for his [Pokedex App design]&#40;https://dribbble.com/shots/6545819-Pokedex-App&#41;)

[//]: # (- [Sabri Bey]&#40;https://www.deviantart.com/sabribey&#41; for the Pikachu gif)

[//]: # (- [Flutter]&#40;https://flutter.dev&#41; for the great cross platform framework)

[//]: # ()
[//]: # (## License)

[//]: # ()
[//]: # (All the code available under the MIT + Apache 2.0. licenses. See [LICENSE]&#40;LICENSE&#41;.)
